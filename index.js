
// functions in javascript are lines or blocks of codes that tells our device 
// to perform a certain task when called/invoke.


function printInput(){
    let nickname = prompt("Enter your nickname:");
    console.log("Hi " + nickname);
}

// function expression does not allow to be hoisted while function declaration allows hoisting.

// printInput();


// Parameters and Arguments
// However, for some use cases, this may not be ideal.
    //parameters is the named variable that is accepted by the declared function
    function printName(name){
        console.log("My name is " + name);
    }


    //sets a default value for the parameter.
    function printName2(name="Mark"){
        console.log("My name is " + name);
    }

    printName("Chris"); //argument is the value that is passed to the function that serves as its parameter.
    printName("George");


    // You can directly pass data into the function. The function can then use that data which is referrec as "name"
    // within the function.

    // "Chris", the information/data provided directly into the function called an argument.

    // Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within
    // the function.


    // variables can also be passed as an argument. 

    let sampleVariable = "Edward";

    printName(sampleVariable);

    // function arguments cannot be used by a function if there are no parameters provided within the declaration of the function.

    function noParams(){
        let params = "No Parameter";
        console.log(params);
    }

    noParams("With parameter!"); //the argument doesnt affect the function and still executes.

    function checkDivisibilityBy8(num){
        
        let remainder = (num%8);
        let isDivisibleBy8 = remainder ===0;
        console.log("The remainder of " + num + " divided by 8 is: " + remainder + ". \nWas it divisible by 8: " +isDivisibleBy8);
    }

    checkDivisibilityBy8(25);
    checkDivisibilityBy8(16);


    // Function as arguments
        // function parameters can also accept other functions as arguments.
        // Some complex functions use other functions as arguments to perform more complicated results.
        // This will be further seen when we discuss arrays methods. 

        function argumentFunction(){
            console.log("This function was passed as an argument before the message was printed.");
        }

        function invokeFunction(argFunction){
            argFunction();
        }

        // If a function is intended to accept an argument
        invokeFunction(argumentFunction);


        // Adding and removing the parentheses "()" impacts the output of JavaScript heavily.
        // When function is used with parentheses "()", it denotes invoking a function.

        // A function used without parentheses "()" is normally associated with using the function
        // as an argument to another function.
        // Using Multiple Parameters
        // Multiple "arguments" will correspond to the number of "parameters declared in a function in 
        // "succeeding order" ." 

        function createFullName(firstName, middleName, lastName){
            console.log("This is firstName: " + firstName);
            console.log("This is middleName: " + middleName);
            console.log("This is lastName: " + lastName);
        }
        
        createFullName("Juan", "Dela", "Cruz", "Jr.");

        createFullName("Juan","Dalisay");

        // "Juan" will be stored in the parameter "firstName"

        // Providing less arguments than the expected parameters will automatically assign an udefined value to the parameter.
        createFullName("Juan", "Dela Cruz");

        // using variables as arguments.


        let firstName = "John";
        let middleName = "Doe";
        let lastName = "Smith";

        createFullName(firstName, middleName, lastName);

        
        // return statement
            // The "return" statement allows us to output a value from a function to be passed to the line/block of code
            // that invoked the function.

            function returnFullName(firstName, middleName, lastName){

                return firstName + " " + middleName + " " + lastName;

            }

            console.log(returnFullName(firstName, middleName, lastName));
            let a = returnFullName(firstName, middleName, lastName);
            console.log(a);
            

            function printPlayerInfo(username, level, job){
                console.log("Username: " + username);
                console.log("Level: " + level);
                console.log("Job: "+ job);
            
                return username + " " + level + " " + job;
            }

            let user1 = printPlayerInfo("knight_white", 95, "Paladin");
            console.log(user1);




        